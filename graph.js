/**
 * Created by akainz on 03/08/15.
 */

var _= require ('underscore');

var argv = require('minimist')(process.argv.slice(2));
var fs = require('fs');

var res='';
function l(text){
	if (arguments.length>0)
		res+=text;
	res+='\n';
}

if (argv.length==0){
	console.error('Please call node MQGraph --in<inputFile.json> ');
	process.exit(-1);
}
console.log('Loading input file '+argv['in']);
var data= fs.readFileSync(argv['in']);

var	source = JSON.parse(data);
var clusters= source.clusters;
var connects= source.connects;

var graph =source.hasOwnProperty('graph') ? source.graph:{};

function toAttributes(object){
	return _.reduce(object, function(memo, value, key){
		if (key=='nodeheight')
			return memo;
		return memo+key+'="'+value+'", ';
	}, '');
}
var nodeheight=24;
var options=toAttributes(graph);
if (options.hasOwnProperty("nodeheight")){
	nodeheight=options['nodeheight'];
}
l('digraph mq {');
l('\tgraph[ overlap=false, splines="ortho", rankdir="TB" '+ options+']');
_.each(clusters, function(  cluster, clusterName) {
	cluster.nodes= 	_.map(cluster.nodes, function(node, index){
		if (typeof node== 'string'){
			node={
				"id": node,
				"style":"queue"};
		}
		if (_.isUndefined(node.label))
			node.label=node.id;
		if (_.isUndefined(node.style))
			node.style='queue';
		return node;
	});
});
_.each(clusters, function(  cluster, clusterName){
	l('{');
	l('\tsubgraph '+clusterName+ '{');
	var clusterStyle;
	if (_.isUndefined(cluster.id))
		cluster.id=clusterName;
	if (cluster.hasOwnProperty('style')){
		if (cluster.style=='system')
			clusterStyle=styleClusterSystem(cluster);
		else
			clusterStyle=styleClusterDomain(cluster);
	}
	else clusterStyle='style=invis';

	l(clusterStyle);


	// Add the nodes
	_.each(cluster.nodes, function(node, index){

		var nodeString;
		if (node.style=='queue')
			nodeString=styleQueue(node);
		if (node.style=='system')
			nodeString=styleSystem(node);
		if (node.style=='subsystem')
			nodeString=styleSubSystem(node);

		l(''+nodeString+'');
	})
	// Add a bottom Node
	l('bottom_'+clusterName+' [style="invis"];');
	l('}');
	// Add some invisible connects that keep the cluster together
	var names=_.map(cluster.nodes, function( node, key){
		return node.id;
	});
	names.push('bottom_'+clusterName);
	l(
		_.reduce(names, function(memo, value, index, list){
			var s=' '+ memo+   ' -> "' + value+ '" [style="invis"];\n \t';
			if (index<list.length-1) {
				s += '"'+value+'"';
			}
			return s;
		}
		)
	);

	l('}');

});
l();
// Add the connects
_.each(connects, function(connect, index) {
		l('"'+connect[0]+'" -> "'+ connect[1]+'" '+(connect.length==3?'['+toAttributes(connect[2])+']':''));
	}
);
l();

// Add a root node and connect to the cluster top nodes
l('0 [style="invis"]');

l();

_.each(clusters, function(  cluster, index){
	var names=_.map(cluster.nodes, function( node, index){
		return node.id;
	});
	l('0 -> "'+names[0]+ '" [style="invis", rank="source", weight="999"]');
});

var clusterNames=_.map(clusters, function( value, key){
	return key;
});
// Add a invisible node to the bottom of each cluster, so the boxes extend all the way
// Made nodes in the cluster align wrongly, removed for now
//_.each(clusterNames, function(  cluster, index){
	//if (!_.isUndefined(clusterNames[index+1]))
	//	l('bottom_'+clusterNames[index]+' -> "bottom_'+clusterNames[index+1]+ '" [style="invis", rank="min"]');
//});


l('}\n');

var outputFile= argv['in'].replace('.json', '.dot');
console.log('Writing dot file '+outputFile);
fs.writeFileSync(outputFile, res);


function styleSystem(node){
	return '"'+node.id+'"[shape=plaintext,  label=<\n'+
		'<TABLE BORDER="1" CELLBORDER="1" CELLSPACING="0" BGCOLOR="#74a0da" >\n'+
	'<TR><TD WIDTH="20" HEIGHT="'+nodeheight+'" FIXEDSIZE="TRUE" ><FONT COLOR="white" FACE="Helvetica Bold"><B>A</B></FONT></TD>\n'+
	'<TD WIDTH="220" HEIGHT="'+nodeheight+'" ><FONT COLOR="white" FACE="Helvetica Bold"><B>&lt;&lt;System&gt;&gt;</B></FONT></TD></TR>\n'+
	'<TR><TD COLSPAN="2" WIDTH="240" ><FONT  FACE="Helvetica Bold"><B>'+node.label+'</B></FONT></TD></TR>\n'+
	'</TABLE>>\n'+
']\n'
}
function styleSubSystem(node){
	return '"'+node.id+'"[shape=plaintext,  label=<\n'+
		'<TABLE BORDER="1" CELLBORDER="1" CELLSPACING="0" BGCOLOR="#74a0da" >\n'+
		'<TR><TD WIDTH="20" HEIGHT="'+nodeheight+'" FIXEDSIZE="TRUE" ><FONT COLOR="white" FACE="Helvetica Bold"><B>A</B></FONT></TD>\n'+
		'<TD WIDTH="220" HEIGHT="'+nodeheight+'" ><FONT COLOR="white" FACE="Helvetica Bold"><B>&lt;&lt;SubSystem&gt;&gt;</B></FONT></TD></TR>\n'+
		'<TR><TD COLSPAN="2" WIDTH="240" ><FONT  FACE="Helvetica Bold"><B>'+node.label+'</B></FONT></TD></TR>\n'+
		'</TABLE>>\n'+
		']\n'
}

function styleQueue(node){
	return '"'+node.id+'" [shape=plaintext,  label=<\n'+
	'<TABLE BORDER="1" CELLBORDER="1" CELLSPACING="0" BGCOLOR="#43b72d" >\n'+
	'<TR><TD WIDTH="20" HEIGHT="'+nodeheight+'" FIXEDSIZE="TRUE" ><FONT COLOR="white" FACE="Helvetica Bold"><B>I</B></FONT></TD>\n'+
	'<TD WIDTH="320" HEIGHT="'+nodeheight+'" ><FONT COLOR="white" FACE="Helvetica Bold"><B>&lt;&lt;Queue&gt;&gt;</B></FONT></TD></TR>\n'+
	'<TR><TD COLSPAN="2" WIDTH="340" ><B><FONT FACE="Helvetica Bold">'+node.label+'</FONT></B></TD></TR>\n'+
	'</TABLE>>\n'+
	']\n'

}

function styleClusterDomain(cluster){
	if (cluster.label=='')
		return 'label=""';

	return 'label=<<TABLE BORDER="1" CELLBORDER="1" CELLSPACING="0" BGCOLOR="#43b72d" >\n'+
	'<TR><TD WIDTH="20" HEIGHT="'+nodeheight+'" FIXEDSIZE="TRUE" ><FONT COLOR="white" FACE="Helvetica Bold"><B>I</B></FONT></TD>\n'+
	'<TD WIDTH="320" HEIGHT="'+nodeheight+'" ><FONT COLOR="white" FACE="Helvetica Bold"><B>&lt;&lt;'+cluster.id+'&gt;&gt;</B></FONT></TD></TR>\n'+
	'<TR><TD COLSPAN="2" WIDTH="340" ><FONT  FACE="Helvetica Bold"><B>'+cluster.label+'</B></FONT></TD></TR>\n'+
	'</TABLE>>\n';
}

function styleClusterSystem(cluster){
	return 'label=<<TABLE BORDER="1" CELLBORDER="1" CELLSPACING="0" BGCOLOR="#74a0da" >\n'+
		'<TR><TD WIDTH="20" HEIGHT="'+nodeheight+'" FIXEDSIZE="TRUE" ><FONT COLOR="white" FACE="Helvetica Bold"><B>A</B></FONT></TD>\n'+
		'<TD WIDTH="320" HEIGHT="'+nodeheight+'" ><FONT COLOR="white" FACE="Helvetica Bold"><B>&lt;&lt;'+cluster.id+'&gt;&gt;</B></FONT></TD></TR>\n'+
		'<TR><TD COLSPAN="2" WIDTH="340" ><FONT  FACE="Helvetica Bold"><B>'+cluster.label+'</B></FONT></TD></TR>\n'+
		'</TABLE>>\n';
}



